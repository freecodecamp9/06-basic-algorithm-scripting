//[SECTION] Convert Celsius to Fahrenheit
	/* The formula to convert from Celsius to Fahrenheit is the temperature in Celsius times 9/5, plus 32.

		ACTIVITY: You are given a variable celsius representing a temperature in Celsius. Use the variable fahrenheit already defined and assign it the Fahrenheit temperature equivalent to the given Celsius temperature. Use the formula mentioned above to help convert the Celsius temperature to Fahrenheit.
	*/
		function convertCtoF(celsius) {
		  let fahrenheit = celsius * (9/5) + 32;
		  return fahrenheit;
		}

		convertCtoF(30);	


//[SECTION] Reverse a String
	/* ACTIVITY: Reverse the provided string. You may need to turn the string into an array before you can reverse it. Your result must be a string.
	*/
		function reverseString(str) {
		  let reversedStr = "";
		  for (let i = str.length - 1; i >= 0; i--) {
		    reversedStr += str[i];
		  }
		  return reversedStr;
		}

		reverseString("hello");


//[SECTION] Factorialize a Number
	/*	Return the factorial of the provided integer.If the integer is represented with the letter n, a factorial is the product of all positive integers less than or equal to n.

	Factorials are often represented with the shorthand notation n!

		For example: 5! = 1 * 2 * 3 * 4 * 5 = 120

	Only integers greater than or equal to zero will be supplied to the function.
	*/
		function factorialize(num) {
		  let product = 1;
		  for (let i = 2; i <= num; i++) {
		    product *= i;
		  }
		  return product;
		}

		factorialize(5);


//[SECTION] Find the Longest Word in a String
	/* ACTIVITY: Return the length of the longest word in the provided sentence. Your response should be a number.
	*/
		function findLongestWordLength(str) {
		  let words = str.split(' ');
		  let maxLength = 0;

		  for (let i = 0; i < words.length; i++) {
		    if (words[i].length > maxLength) {
		      maxLength = words[i].length;
		    }
		  }

		  return maxLength;
		}

		findLongestWordLength("The quick brown fox jumped over the lazy dog");

		//CODE EXPLANATION
			/* Take the string and convert it into an array of words. Declare a variable to keep track of the maximum length and loop from 0 to the length of the array of words. Then check for the longest word by comparing the current word to the previous one and storing the new longest word. At the end of the loop just return the number value of the variable maxLength.
			*/


//[SECTION] Return Largest Numbers in Arrays
	/* ACTIVITY: 
		Return an array consisting of the largest number from each provided sub-array. For simplicity, the provided array will contain exactly 4 sub-arrays.

		Remember, you can iterate through an array with a simple for loop, and access each member with array syntax arr[i].
	*/
		function largestOfFour(arr) {
		  const results = [];
		  for (let i = 0; i < arr.length; i++) {
		    let largestNumber = arr[i][0];
		    for (let j = 1; j < arr[i].length; j++) {
		      if (arr[i][j] > largestNumber) {
		        largestNumber = arr[i][j];
		      }
		    }
		    results[i] = largestNumber;
		  }

		  return results;
		}

		largestOfFour([[4, 5, 1, 3], [13, 27, 18, 26], [32, 35, 37, 39], [1000, 1001, 857, 1]]);

			//CODE EXPLANATION
				/* 
					- Create a variable to store the results as an array.
					- Create an outer loop to iterate through the outer array.
					- Create a second variable to hold the largest number and initialise it with the first number. This must be outside an inner loop so it won’t be reassigned until we find a larger number.
					- Create said inner loop to work with the sub-arrays.
					- Check if the element of the sub array is larger than the currently stored largest 	number. If so, then update the number in the variable.
					- After the inner loop, save the largest number in the corresponding position inside of the results array.
					- And finally return said array.
				*/


//[SECTION] Confirm the Ending
	/* ACTIVITY: Check if a string (first argument, str) ends with the given target string (second argument, target). This challenge can be solved with the .endsWith() method, which was introduced in ES2015. But for the purpose of this challenge, we would like you to use one of the JavaScript substring methods instead.
	*/
		function confirmEnding(str, target) {
		  // "Never give up and good luck will find you."
		  // -- Falcor

		  return str.slice(str.length - target.length) === target;
		}

		confirmEnding("He has to give me a new name", "name");

			//CODE EXPLANATION
				/* 
					- First we use the slice method copy the string.
					- In order to get the last characters in str equivalent to the target's length we use the slice method.
					- The first parameter inside the slice method is the starting index and the second parameter would be the ending index.
					- For example str.slice(10, 17) would return give me.
					- In this case we only include one parameter which it will copy everything from the starting index.
					- We substract the length of str and the length of target, that way, we shall get the last remaining characters equivalent to the target's length.
					- Finally we compare the return result of slice to target and check if they have the same characters.
				*/


//[SECTION] Confirm the Ending
	/* Check if a string (first argument, str) ends with the given target string (second argument, target).

	This challenge can be solved with the .endsWith() method, which was introduced in ES2015. But for the purpose of this challenge, we would like you to use one of the JavaScript substring methods instead.
	*/

	function confirmEnding(str, target) {
	  return str.slice(str.length - target.length) === target;
	}
	confirmEnding("Bastian", "n");


//[SECTION] Repeat a String Repeat a String
	/* Repeat a given string str (first argument) for num times (second argument). Return an empty string if num is not a positive number. For the purpose of this challenge, do not use the built-in .repeat() method.
	*/
	function repeatStringNumTimes(str, num) {
	  let accumulatedStr = "";

	  for (let i = 0; i < num; i++){
	  	 accumulatedStr += str;
	  }
	    return accumulatedStr;
	}
	repeatStringNumTimes("abc", 3);


//[SECTION] Truncate a String
	/* 
		Truncate a string (first argument) if it is longer than the given maximum string length (second argument). Return the truncated string with a ... ending

		Hint 1:
		Strings are immutable in JavaScript so we will need a new variable to store the truncated string.

		Hint 2:
		You will need to use the slice() method and specify where to start and where to stop.
	*/
	function truncateString(str, num) {
	  // Clear out that junk in your trunk
	  if (str.length > num) {
	    return str.slice(0, num) + "...";
	  } else {
	    return str;
	  }
	}
	truncateString("A-tisket a-tasket A green and yellow basket", 8);	
	
//[SECTION] Finders Keepers
	/*
		Create a function that looks through an array arr and returns the first element in it that passes a 'truth test'. This means that given an element x, the 'truth test' is passed if func(x) is true. If no element passes the test, return undefined.

		Code Explanation:
			Look through the array given in the 1st paramater “arr” using the .map() method
			Use the function in the 2nd parameter as the callback function in arr.map()
			Acquire the index of the first number that meets the condition in the function.
			Use that index to display the first available number that meets the condition.
	*/
	function findElement(arr, func) {
	  return arr[arr.map(func).indexOf(true)];
	}
	findElement([1, 2, 3, 4], num => num % 2 === 0);


//[SECTION] Boo who
	/*
		Check if a value is classified as a boolean primitive. Return true or false.
		Boolean primitives are true and false.
	*/
	function booWho(bool) {
	 return typeof bool === "boolean"; 
	}
	booWho(null);


//[SECTION] Title Case a Sentence
	/*
		Return the provided string with the first letter of each word capitalized. Make sure the rest of the word is in lower case. For the purpose of this exercise, you should also capitalize connecting words like the and of.

		CODE EXPLANATION:
			Split the string by white spaces, and create a variable to track the updated title. Then we use a loop to turn turn the first character of the word to uppercase and the rest to lowercase. By creating concatenated string composed of the whole word in lowercase with the first character replaced by its uppercase.
	*/
	function titleCase(str) {
	  const newTitle = str.split(" ");
	  const updatedTitle = [];
	  for (let st in newTitle) {
	    updatedTitle[st] = newTitle[st][0].toUpperCase() + newTitle[st].slice(1).toLowerCase();
	  }
	  return updatedTitle.join(" ");
	}

//[SECTION] Slice and Spice
	/*
		You are given two arrays and an index. Copy each element of the first array into the second array, in order. Begin inserting elements at index n of the second array. Return the resulting array. The input arrays should remain the same after the function runs.

		CODE EXPLANATION:
			Since our goal is to return the new array with out altering arr1 or arr2 we create a localArr and add all the items from arr2 using the slice() function

			Since the splice() function will mutate (alter) arrays and can be used to add new elements we will use it to add the contents of arr1 into localArr. n is the starting position where our content will be inserted. We won’t be deleting any elements so the next argument is 0. Then we add the entire contents of arr1 using spread syntax ....

			localArr is returned and the function is complete
	*/
		function frankenSplice(arr1, arr2, n) {
		  // It's alive. It's alive!
		  let localArr = arr2.slice();
		  localArr.splice(n, 0, ...arr1);
		  return localArr;
		}
		frankenSplice([1, 2, 3], [4, 5, 6], 1);


//[SECTION] Falsy Bouncer
	/* Remove all falsy values from an array.
		Hint 1
		Falsy is something which evaluates to FALSE. There are only six falsy values in JavaScript: undefined, null, NaN, 0, “” (empty string), and false of course.

		Hint 2
		We need to make sure we have all the falsy values to compare, we can know it, maybe with a function with all the falsy values…

		Hint 3
		Then we need to add a filter() with the falsy values function…

	CODE EXPLANATION:
		- We create a new empty array.
		- We use a for cycle to iterate over all elements of the provided array (arr).
		- We use the if statement to check if the current element is truthy 2.7k or falsy 4.3k.
		- If the element is truthy, we push it to the new array (newArray). This result in the new array (newArray) containing only truthy elements.
		- We return the new array (newArray).
	*/

	function bouncer(arr) {
	  let newArray = [];
	  for (let i = 0; i < arr.length; i++) {
	    if (arr[i]) newArray.push(arr[i]);
	  }
	  return newArray;
	}
	bouncer([7, "ate", "", false, 9]);


//[SECTION] Where do I Belong
	/*
		Return the lowest index at which a value (second argument) should be inserted into an array (first argument) once it has been sorted. The returned value should be a number.

		For example, getIndexToIns([1,2,3,4], 1.5) should return 1 because it is greater than 1 (index 0), but less than 2 (index 1).

		Likewise, getIndexToIns([20,3,5], 19) should return 2 because once the array has been sorted it will look like [3,5,20] and 19 is less than 20 (index 2) and greater than 5 (index 1).

	Hint 1
		The first thing to do is sort the array from lower to bigger, just to make the code easier. This is where sort comes in, it needs a callback function so you have to create it.

	Hint 2
		Once the array is sorted, then just check for the first number that is bigger and return the index.

	Hint 3
		If there is no index for that number then you will have to deal with that case too.

	Code Explanation:
		- Count the number of entries that are smaller than the new value num
		- The new value would be inserted after these values
	*/
	function getIndexToIns(arr, num) {
	  return arr.filter(val => num > val).length;
	}
	getIndexToIns([40, 60], 50);


//[SECTION] Mutations
	/*
		Return true if the string in the first element of the array contains all of the letters of the string in the second element of the array.

		For example, ["hello", "Hello"], should return true because all of the letters in the second string are present in the first, ignoring case.

		The arguments ["hello", "hey"] should return false because the string hello does not contain a y.

		Lastly, ["Alien", "line"], should return true because all of the letters in line are present in Alien.

	Hint 1
		If everything is lowercase it will be easier to compare.

	Hint 2
		Our strings might be easier to work with if they were arrays of characters.

	Hint 3
		A loop might help. Use indexOf() to check if the letter of the second word is on the first.


	CODE EXPLANATION:
		Grab the second string, lowercase and turn it into an array; then make sure every one of its letters is a part of the lowercased first string.

		Every will basically give you letter by letter to compare, which we do by using indexOf on the first string. indexOf will give you -1 if the current letter is missing. We check that not to be the case, for if this happens even once every will be false.
	*/
	function mutation(arr) {
	  return arr[1]
	    .toLowerCase()
	    .split("")
	    .every(function(letter) {
	      return arr[0].toLowerCase().indexOf(letter) !== -1;
	    });
	}
	mutation(["hello", "hey"]);


//[SECTION] Chunky Monkey
	/*
		Write a function that splits an array (first argument) into groups the length of size (second argument) and returns them as a two-dimensional array.

		CODE EXPLANATION:
			Firstly, we create two variables. newArr is an empty array which we will push to. We also have the i variable set to zero, for use in our while loop.

			Our while loop loops until i is equal to or more than the length of the array in our test.

			Inside our loop, we push to the newArr array using arr.slice(i, i+size). For the first time it loops, it will look something like:

			newArr.push(arr.slice(1, 1+2))

			After we push to newArr, we add the variable of size onto i.

			Finally, we return the value of newArr.
	*/
	function chunkArrayInGroups(arr, size) {
	  // Break it up.
	  let newArr = [];
	  let i = 0;

	  while (i < arr.length) {
	    newArr.push(arr.slice(i, i + size));
	    i += size;
	  }
	  return newArr;
	}
	chunkArrayInGroups(["a", "b", "c", "d"], 2);